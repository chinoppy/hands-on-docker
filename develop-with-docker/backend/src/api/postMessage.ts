import { Request, Response } from 'express'
import { Message } from '../entity/message'
import { getRepository } from '../db/connection'

export default async (req: Request, res: Response): Promise<Response> => {
  const message: Message = req.body
  console.log(message)
  const repository = await getRepository(Message)
  const result = await repository.insert(message)
  return res.status(200).send(result)
}
