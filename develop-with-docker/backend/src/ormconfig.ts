import { ConnectionOptions } from 'typeorm'

const config: ConnectionOptions = {
  name: 'default',
  type: 'postgres',
  host: 'postgres',
  port: 5432,
  username: 'postgres',
  password: 'Tu6Ws4nc9J8a5eMeRBJ69G92XX',
  database: 'postgres',
  synchronize: false,
  logging: true,
  entities: ['src/entity/**/*.ts'],
  migrations: ['src/migration/**/*.ts'],
  subscribers: ['src/subscriber/**/*.ts'],
}

export default config
module.exports = config
